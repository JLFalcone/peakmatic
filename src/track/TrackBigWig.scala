/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

import java.io._

import org.broad.igv.bbfile._
import scala.util.{Try,Success,Failure}

import scala.collection.JavaConverters._

object TrackBigWig {

  def read( filename: String ): Try[Track] = Try {
    val reader = new BBFileReader( filename )
    val bbFileHdr = reader.getBBFileHeader()
    if( ! bbFileHdr.isHeaderOK ) {
      throw new IOException("bad header for "+filename)
    }
    if( ! bbFileHdr.isBigWig ) {
      throw new IOException("This is not a big wig file: " + filename )
    }
    val tb = new TrackBuilder( "NA", "NA" )
    for( it <- reader.getBigWigIterator.asScala ) {
      val dp = DataPoint(
	Chromosome( it.getChromosome ),
	it.getStartBase,
	it.getEndBase,
	it.getWigValue
      )
      tb.add( dp )
    }
    tb.build
  }

  def read( file: File ): Try[Track] = read( file.getAbsolutePath )

}
