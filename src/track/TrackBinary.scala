/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/*package ch.unige.peakmatic
package track

import scala.util.Try
import sbinary._

object TrackBinary {

  import TrackBinaryProtocol._
  import Operations._
  import java.io._
  import java.util.zip._

  def read( filename: String ): Try[Track] = Try { 
    val in = new GZIPInputStream( new FileInputStream( filename ) )
    val track = Operations.read[Track](in)
    in.close
    track
  }

  def write( filename: String, track: Track ): Try[Unit] = Try { 
    val out = new GZIPOutputStream( new FileOutputStream( filename ) )
    val ary = toByteArray( track )
    out.write( ary )
    out.close
  }

  def readNode( filename: String ): Try[Node] = Try { 
    fromFile[Node]( new java.io.File(filename ) )
  }

  def writeNode( filename: String, node: Node ): Try[Unit] = Try { 
    toFile(node)( new java.io.File(filename) )
  }

}

object TrackBinaryProtocol extends DefaultProtocol {

  implicit val ChromosomeFMT = 
    wrap[Chromosome, String](_.name, Chromosome.apply)

  implicit val NodeFMT: Format[Node] = lazyFormat(
    asUnion[Node]( DataNodeFMT, BinaryNodeFMT )
  )
    
  implicit val DataNodeFMT: Format[DataNode] = 
    asProduct2( DataNode )( DataNode.unapply(_).get )

  implicit val BinaryNodeFMT: Format[BinaryNode] = 
    asProduct2( BinaryNode.apply )( BinaryNode.unapply(_).get )

  implicit val TrackFMT: Format[Track] = 
    asProduct3( Track )( Track.unapply(_).get )

}
 */
