/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

import java.io._

object DumpData {

  def trackAndPeaks( 
    outFN: String, 
    full: Track, 
    peaks: Track, 
    chr: Chromosome, 
    start: Long, 
    length: Int 
  ): Unit = {
    val nF = full.data(chr)
    val nP = peaks.data(chr)
    val out = new PrintWriter( outFN )
    var i = start
    while( i <= (start+length) ) {
      val iF = nF.get(i).getOrElse(0.0)
      val iP = nP.get(i).getOrElse(0.0)
      ( iF != 0.0, iP != 0.0 ) match {
	case (true,true)  => out.println( s"$i $iF $iP" )
	case (false,true) => out.println( s"$i NA $iP" )
	case (true,false) => out.println( s"$i $iF NA" )
	case _ => 
      }
      i += 1
    }
    out.close
  }

}
