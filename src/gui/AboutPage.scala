/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui


import scala.swing._
import scala.swing.event._

import java.io.File

object AboutPage {
  def apply( prefs: Prefs ): TabbedPane.Page = {
    new TabbedPane.Page( "About", info )
  }

  lazy val info = new EditorPane("text/html", infoContent) {
    editable = false
    border = Swing.EmptyBorder(30, 30, 30, 30)
  }

  lazy val infoContent = """
<html><body>
<h1>About</h1>

<p>Peakmatic  Copyright © 2014-2019, University of Geneva</p>

<p>This program comes with ABSOLUTELY NO WARRANTY. This is free software,
and you are welcome to redistribute it under certain conditions (see <a href="https://www.gnu.org/licenses/gpl-3.0.html">GPLv3</a>)</p>.

<p>Please report any issue to <a href="https://gitlab.unige.ch">the project site.</a></p> 

</body></html> """
}
