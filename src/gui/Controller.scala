/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui

import scala.swing._
import scala.swing.event._

import java.io.File

import util.Konsole


trait Controller[M <: Model[M]] {

  protected def prefs: Prefs

  protected def freshModel: M

  protected def redraw( model: M ): Unit 

  protected def saveParameters( model: M ): Unit

  def formPanel: Panel
  
  private var model: M = freshModel

  lazy val resetButton = new Button {
    text = "Reset"
    tooltip = "Reset the parameters to default values."
  }
  lazy val saveButton = new Button {
    text = "Save param"
    tooltip = "Save the current parameters as default values."
  }
  lazy val runButton = new Button {
    text = "RUN"
    tooltip = "Run the program"
  }

  lazy val konsole = new TextArea( 5, 60 ) {
    editable = false
  }

  implicit protected lazy val tak: Konsole = new TextAreaKonsole( konsole )

  def updateModel( f: M=>Either[String,M], errType: String, parent: Component ): Unit = {
    f(model) match {
      case Left(msg) => Dialog.showMessage( 
	parent = parent, 
	message = msg,
	title = errType,
	messageType = Dialog.Message.Error
      )
      case Right(m) => model = m
    }
  }
 

  protected def makeInFC( dir: String ) = new FileChooser( new java.io.File(dir) ) {
    fileSelectionMode = FileChooser.SelectionMode.FilesOnly
  }
  protected def makeOutFC( dir: String ) = new FileChooser( new java.io.File(dir) ) {
    fileSelectionMode = FileChooser.SelectionMode.FilesOnly
  }

  def doRedraw(): Unit = redraw(model)

  def top = new BoxPanel( Orientation.Vertical ) {
    contents += formPanel
    contents += new FlowPanel {
      contents += resetButton
      contents += saveButton
      contents += runButton
    }
    contents += new ScrollPane(konsole) {
      border = Swing.EmptyBorder(30, 30, 30, 30)
    }
    
    listenTo(resetButton,saveButton,runButton)


    reactions += {
      case ButtonClicked(`resetButton`) => {
	model = freshModel
	redraw(model)
      }
      case ButtonClicked(`saveButton`) => {
	val res = Dialog.showConfirmation(
	  parent = contents.head,
	  title = "Saving parameters as default",
	  message = "Do you want to save the current parameters as default ?"
	)
	if( res == Dialog.Result.Yes ) {
	  saveParameters(model)
	}
      }
      case ButtonClicked(`runButton`) => {
	model.run() match {
	  case Right(_) =>
    	    Dialog.showMessage( 
	      parent = contents.head, 
	      message = "Operation has been completed.", //TODO: Write better msg
	      title = "Operation Success",
	      messageType = Dialog.Message.Info
	    )
	    model = model.resetFiles
	  case Left( msg ) => Dialog.showMessage( 
	    parent = contents.head, 
	    message = msg,
	    title = "Operation Failure",
	    messageType = Dialog.Message.Error
	  )
	}
	redraw(model)
      }
    }

    redraw(model)
    
  }


}
