/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package  ndr


case class Region( from: Long, to: Long ) {
  def isEmpty: Boolean = from == to

  def overlaps( that: Region ): Boolean =
    ( this.from < that.to && this.from >= that.from ) ||
  ( that.from < this.to && that.from >= this.from ) ||
  ( that.from < this.from && that.to >= this.to ) ||
  ( this.from < that.from && this.to >= that.to )

  def union( that: Region ): Region = {
    if( that.isEmpty ) this
    else if( this.isEmpty ) that
    else {
      val f = from.min(that.from)
      val t = to.max(that.to)
      Region( f, t )
    }
  }

  def intersect( that: Region ): Region = {
    val f = from.max( that.from )
    val t = to.min( that.to )
    if( f < t ) Region( f, t )
    else Region.empty
  }

  def diff( that: Region ): List[Region] = {
    val chunks =
      if( this.to <= that.from || this.from >= that.to )
        List( this )
      else if( this.from <= that.from && this.to >= that.from && that.to >= this.to )
        List( Region( this.from, that.from ) )
      else if( this.from >= that.from && this.from <= that.to && this.to >= that.to )
        List( Region( that.to, this.to ) )
      else if( that.from >= this.from && that.to <= this.to )
        List( Region( this.from, that.from), Region( that.to, this.to ) )
      else if( this.from >= that.from && this.to <= that.to )
      Nil
      else sys.error( s"$this $that" )
    chunks.filter( _.size > 0 )
  }



  def size = to - from
}
object Region {
  def empty = Region( 0, 0 )
}

sealed trait Regions {
  import Regions.{Node,Empty}
  def height: Int
  def balanceFactor: Int
  def size:Int = this match {
    case Empty => 0
    case Node( l, _, r ) => l.size + 1 + r.size
  }

  def rotateL: Regions = this match {
    case Node(a, x, Node(b,y,c) ) => Node( Node(a,x,b), y, c )
    case _ => this
  }
  def rotateR: Regions = this match {
    case Node(Node(a,x,b), y, c ) => Node( a, x, Node(b, y, c) )
    case _ => this
  }

  def balance: Regions = this match {
    case Empty => Empty
    case n @ Node( l, v, r ) => ( n.balanceFactor, l.balanceFactor, r.balanceFactor ) match {
      case (2, -1, _ ) => Node( l.rotateL, v, r ).rotateR
      case (2, _, _ ) => n.rotateR
      case (-2, _, 1 ) => Node( l, v, r.rotateR ).rotateL
      case (-2, _, _ ) => n.rotateL
      case _ => n
    }
  }
  def add( region: Region ): Regions = this match {
    case Empty => Node( Empty, region, Empty )
    case Node(l,v,r) => {
      if( region.overlaps(v) ) Node( l, v.union( region ), r )
      else if( region.to < v.from ) Node( l.add(region), v, r ).balance
      else Node( l, v, r.add(region) ).balance
    }
  }

  def intersect( that: Regions, minSize: Int ): Regions = {
    that.toStream.foldLeft( Empty: Regions ){
      case (result,reg) => {
        val cands = intersectR( reg ).filter( _.size >= minSize )
        cands.foldLeft(result)( _ add _ )
      }
    }
  }

  def intersectR( region: Region ): List[Region] = this match {
    case Empty => Nil
    case n @ Node( l, v, r ) => {
      if( region.to <= n.from || region.from >= n.to ) Nil
      else {
        val center = {
          val in = v.intersect( region )
          if( in.isEmpty ) Nil else List(in)
        }
        l.intersectR( region ) ++ center ++ r.intersectR( region )
      }
    }
  }

  def diff( that: Regions, minSize: Int ): Regions = {
    that.toStream.foldLeft( this: Regions ){
      case (result,reg) => {
        Regions.build( result.diffR(reg) )( reg =>
          if( reg.size >= minSize ) Some(reg) else None
        )
      }
    }
  }

  def diffR( region: Region ): List[Region] = this match {
    case Empty => Nil
    case n @ Node( l, v, r ) => {
      if( region.to <= n.from || region.from >= n.to ) n.toStream.toList
      else {
        val center = v.diff(region)
        l.diffR( region ) ++ center ++ r.diffR( region )
      }
    }
  }


  def toStream: Stream[Region] = this match {
    case Empty => Stream.empty
    case Node( l, v, r ) => l.toStream ++ ( v #:: r.toStream )
  }
}

object Regions {

  val empty: Regions = Empty
  def build[A]( ta: Traversable[A] )( f: A=>Option[Region] ): Regions = {
    ta.foldLeft( empty ){
      case (regs,a) => f(a).fold( regs )( regs.add(_) )
    }
  }

  case class Node( left: Regions, current: Region, right: Regions ) extends Regions {
    val height = 1 + left.height.max( right.height )
    val balanceFactor = left.height - right.height
    val from: Long = left match {
      case n: Node => n.from
      case Empty => current.from
    }
    val to: Long = right match {
      case n: Node => n.to
      case Empty => current.to
    }
  }
  case object Empty extends Regions {
    val height = 0
    val balanceFactor = 0
  }
}

