/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

class NodeBuilder() {

  private var offset = -1L

  private var leafs = List[DataNode]()

  private var dataBuilder: DataBuilder = null


  def add( dp: track.DataPoint ): Unit = {
    if( offset < 0 ) {
      offset = dp.start
      dataBuilder = new DataBuilder( offset )
    }
    val continue = dataBuilder.add(dp)
    if( !continue  ) {
      val data = dataBuilder.build
      leafs ::= DataNode( offset, data )
      dataBuilder = null
      offset = -1
      add( dp )
    }
  }

  def add( index: Long, value: Data ): Unit = {
    if( offset < 0 ) {
      offset = index
      dataBuilder = new DataBuilder( offset )
    }
    if( value.isDefined ) {
      val continue = dataBuilder.add(index, value.value)
      if( !continue  ) {
	val data = dataBuilder.build
	leafs ::= DataNode( offset, data )
	dataBuilder = null
	offset = -1
	add( index, value )
      }
    }

  }
/*
  private def split( ary: List[Long] ): Int = {
    def eval( n: Int ): Long = {
      var i = 0
      var sum1 = 0L
      while( i < n ) {
	sum1 += ary(i)
	i += 1
      }
      var sum2 = 0L
      while( i < ary.length ) {
	sum2 += ary(i)
	i += 1
      }
      val diff = sum1 - sum2
      diff*diff
    }
    def findBest( current: Int, best: Int, bestEval: Long ): Int =
      if( current == ary.length ) best
      else {
	val ce = eval( current )
	if( ce < bestEval ) findBest( current+1, current, ce )
	else findBest( current+1, best, bestEval )
      }      

    findBest( 0, -1, Long.MaxValue )
  }
  */  

  private def split( ary: List[Long] ): Int = {
    def findBest( left: Long, right: Long, i: Int, lastMin: Long ): Int = {
      val l = left + ary(i)
      val r = right - ary(i)
      val min = l min r
      //println( s"L $left  R $right  I $i  MIN $min" )
      if( min < lastMin ) i
      else findBest( l, r, i+1, min )
    }
    val res = findBest( 0, ary.sum, 0, 0 )
    //println( s"LIST: $ary result: $res sum: ${ary.sum}" )
    res
  }

  private def buildTree( lst: List[Node], n: Int = 0 ): Node = 
    if( n > 100 ) ???
    else {
      lst match {
	case Nil => EmptyNode
	case n :: Nil => n
	case m :: n :: Nil => BinaryNode( m, n )
	case _ => {
	  val sizes: List[Long] = lst.map{ _.size }
	  val i = split( sizes )
	  val left = buildTree( lst.take(i), n+1 )
	  val right = buildTree( lst.drop(i), n+1 )
	  BinaryNode(left,right)
	}
      }
    }

  def build(): Node = {
    if( offset >= 0 ) {
      val data = dataBuilder.build
      leafs ::= DataNode( offset, data )
      dataBuilder = null
      offset = -1
    }
    buildTree( leafs.reverse )
  }

}

class DataBuilder( offset: Long ) {


 private var currentData = Array.ofDim[Double]( 1024 )

  private var nextIdx = 0
  private var lastPos = -1

  private def ensureCapacity( size: Int ) {            //FIXME: MAX SIZE
    if( currentData.length - 1 < size ) {
      val next = Array.ofDim[Double]( currentData.length * 2  )
      Array.copy( currentData, 0, next, 0, nextIdx )
      currentData = next
      //prinln( s"Curr cap = ${next.size}" )
    }
  }

  def add( dp: track.DataPoint ): Boolean = {
    ensureCapacity( (dp.end-offset).toInt )
    val first = dp.start
    if( first == nextIdx ) fill( dp )
    else if( first > nextIdx ) insertBlanks( dp )
    else ???
  }

  def add( index: Long, value: Double ): Boolean = {
    val idx = (index-offset).toInt
    if( idx - nextIdx > Config.maxBlank ) false
    else {
      ensureCapacity( idx )
      insertBlanks( idx-nextIdx )
      currentData( idx ) = value
      nextIdx += 1
      true
    }
  }


  def build(): Array[Double] = {
    val next = Array.ofDim[Double]( nextIdx )
    Array.copy( currentData, 0, next, 0, nextIdx )
    currentData = null //TODO: Ugly
    next
  }

  private def fill( dp: track.DataPoint ): Boolean = { 
    var n = dp.end - dp.start
    while( n > 0 ) {
      currentData( nextIdx ) = dp.value
      nextIdx += 1
      n -= 1
    }
    true
  }

  private def insertBlanks( dp: track.DataPoint ): Boolean = { 
    var n = ( (dp.start-offset) - nextIdx ).toInt
    if( n > Config.maxBlank ) false
    else {
      insertBlanks(n)
      fill( dp )
    }
  }

  private def insertBlanks( num: Int ): Unit = {
    var n = num
    while( n > 0 ) {
      currentData( nextIdx ) = Double.NaN
      nextIdx += 1
      n -= 1
    }
  }
      


}

