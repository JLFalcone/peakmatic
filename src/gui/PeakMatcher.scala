/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui

import scala.util.{Try,Success,Failure}
import java.io.File
import track._

case class PeakMatcher( 
  input1: Option[File]=None,
  input2: Option[File]=None,
  output: Option[File]=None,
  limit: Int,
  minRatio: Double
)( implicit K: util.Konsole ) extends Model[PeakMatcher] {
  
  def setInput1( f: File ): Either[String,PeakMatcher] = 
    setInFile( f )( copy( input1=Some(f) ) )

  def setInput2( f: File ): Either[String,PeakMatcher] = 
    setInFile( f )( copy( input2=Some(f) ) )
  
  def setOutput( f: File ): Either[String,PeakMatcher] = 
    setOutFile( f )( copy( output=Some(f) ) )

  def setLimit( s: String ): Either[String,PeakMatcher] = {
    Try {
      s.toInt
    } match {
      case Success(i) if i > 0 => Right( copy( limit=i ) )
      case Success(i) => Left( s"Limit must be positive, received $i." )
      case Failure(_) => Left( s"Cannot convert $s to an integer." )
    }      
  }

  def setMinRatio( s: String ): Either[String,PeakMatcher] = {
    Try {
      s.toDouble
    } match {
      case Success(i) if i > 0 => Right( copy( minRatio=i ) )
      case Success(i) => Left( s"Limit must be positive, received $i." )
      case Failure(_) => Left( s"Cannot convert $s to a number." )
    }      
  }

  def resetFiles: PeakMatcher = copy( input1=None, input2=None, output=None )

  def validate(): Either[String,(File,File,File)] = (input1,input2,output) match {
    case (Some(in1),Some(in2),Some(out)) => Right((in1,in2,out))
    case _ => Left( "Enter all input and output files.")
  }
  

  def run(): Either[String,Unit] = {
    for {
      insOut <- validate().right
      full1 <- tryOpen(insOut._1).right
      full2 <- tryOpen(insOut._2).right
      frag <- Right( PeakDiff.fragileTrack( full1, full2, limit, minRatio ) ).right
      _ <- try2either( TrackWiggle.write( insOut._3, frag, false ) )( _.toString ).right
    } yield ()
  }

}
