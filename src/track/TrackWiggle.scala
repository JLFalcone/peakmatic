/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

import java.io._
import java.util.zip.GZIPInputStream

import ch.unige.peakmatic.util.Konsole

import scala.util.{Try,Success,Failure}

object TrackWiggle {

  private val readName = "wiggle-reader"

  def read( filename: String )( implicit K: Konsole): Try[Track] = 
    read( new File( filename) )

  def read( file: File )( implicit K: Konsole) : Try[Track] = {
    K.info( readName, "trying to open: $file" )
    val res = openStream( file ).flatMap { readTrack }

    res match {
      case Success(_) => K.info( readName, s"Done reading: $file" )
      case Failure(f) => K.error( readName, s"Failure when reading $file (${f.getMessage})" )
    }

    res
    
  }
  
  private val gzipMagic = Array[Byte]( 0x1f.toByte, 0x8b.toByte )


  private def openStream( file: File ): Try[BufferedReader] = {
    val fis = Try{ new FileInputStream( file ) }
    val is = fis.map{ input =>
      val pb = new PushbackInputStream( input, 2 )
      val signature = Array.ofDim[Byte](2)
      pb.read( signature )
      pb.unread( signature )
      if( signature(0) == gzipMagic(0) && signature(1) == gzipMagic(1) )
	new GZIPInputStream( pb )
      else 
	pb
    }
    is.map( in => new BufferedReader(new InputStreamReader(in, "UTF-8") ) )
  }

  private val sep = """\s+"""
  private val header = """track type=\w+ name="([^"]+)" description="([^"]+)"""".r

  private def parseDatapoint( line: String ) = Try {
    val Array( chr, start, end, value ) = line.split(sep)
    DataPoint( Chromosome(chr), start.toLong, end.toLong, value.toDouble )
  }

  private def readTrack( br: BufferedReader ): Try[Track] = {

    def readHeader( in: BufferedReader ): Try[Track] = {
      val ttrack = Try {
	val next = in.readLine
	val (name,desc) = next match {
	  case header(n,d) => (n,d)
	  case _ => ("NA","NA")
	}
	new TrackBuilder( name, desc )
      }
      readLine( in, ttrack )
    }

    def readLine( in: BufferedReader, ttrack: Try[TrackBuilder] ): Try[Track] = ttrack match {
      case Success(_) => {
	val next = br.readLine
	if( next == null ) {in.close; ttrack.map{ _.build } }
	else if( next(0) == '#' ) readLine( in, ttrack )
	else if( next(0) == '-' ) {in.close; ttrack.map{ _.build } }
	else {
	  val tdp = parseDatapoint( next )
	  val tb2 = for{
	    tb <- ttrack
	    dp <- tdp
	  } yield { tb.add(dp); tb }
	  readLine(in,tb2)
	}
      }
      case f @ Failure(_) => in.close; f.asInstanceOf[Try[Track]]
    }

    readHeader( br )
  }

  private val writeName = "wiggle-writer"

  def write( 
    filename: String, track: Track, commentHeader: Boolean = false 
  )( implicit K: Konsole): Try[Unit] =
    write( new File( filename ), track, commentHeader )

  def write( 
    file: File, track: Track, commentHeader: Boolean 
  )( implicit K: Konsole): Try[Unit] = {
    K.info( writeName, s"writing: ${track.name} to $file" )
    for {
      pw <- open( file ) 
      _ <- writeHeader(pw, track, commentHeader )
      _ <- writeData( pw, track )
      _ <- close( pw )
    } yield {}
  }
  
  private def open( file: File ) = Try{ new PrintWriter( file ) }

  private def writeHeader( pw: PrintWriter, track: Track, commentHeader: Boolean ) = Try{
    val cmt = if( commentHeader ) "#" else ""
    pw.println( s"${cmt}track type=wiggle_0 name=${track.name} description=${track.desc}" )
  }

  private def writeData( pw: PrintWriter, track: Track ) = Try{
    for{ 
      chr <- track.chromosomes.toList
    } {
      val node = track.data(chr)
      var i: Long = node.first
      val N: Long = node.last
      while( i < N ) {
	val d = node.get(i)
	if( d.isDefined  ) {
	  pw.println( s"${chr.name}\t$i\t${i+1}\t${d.value}" )
	}
	i += 1
      }
    }
  }

  private def close( pw: PrintWriter )( implicit K: Konsole) = Try{
    pw.close
    K.info( writeName, s"Done writing." )
  }


}
