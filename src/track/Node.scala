/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

sealed trait Node {
  def first: Long
  def last: Long
  def size: Long
  def get( l: Long ): Data

  def merge( that: Node ): Node = {
    if( that.first < this.first ) that.merge(this)
    else BinaryNode.build( this, that )
  }

  def depth: Int
  def hops( l: Long ): Int

  private def asStream( n: DataNode, i: Long ): Stream[(Long,Double)] = {
    if( i >= n.last ) Stream.empty
    else {
      val v = n.get(i)
      if( v.isDefined ) (i,v.value) #:: asStream( n, i+1 )
      else asStream( n, i+1 )
    }
  }

  def toStream: Stream[(Long,Double)] = this match {
    case EmptyNode => Stream.empty
    case BinaryNode(l,r) => l.toStream ++ r.toStream
    case n: DataNode => asStream( n, n.first )
  }

  def slice( from: Long, to: Long ): Node = this match {
    case BinaryNode( l, r ) => {
      if( l.first > to || r.last < from || (from >= l.last && to < r.first ) ) {
	DataNode( from, Array() )
      } else if( l.last >= from && to > r.first ) 
	       BinaryNode.build( l.slice( from, l.last ), r.slice( r.first, to ) )
        else if( l.last >= from ) l.slice(from,l.last)
	else if( to > r.first ) r.slice(r.first,to)
	else EmptyNode
      }
      case dn: DataNode => {
	val fst = from max dn.first 
	val lst = to min dn.last
	//val size = (lst-fst).toInt
	val i = (fst-dn.first).toInt
	val j = (lst-fst).toInt
	val d = dn.data.slice( i, j )
	DataNode( fst, d )
      }
      case EmptyNode => EmptyNode
    }
}


case object EmptyNode extends Node {
  val first = -1L
  val last = -1L
  val size = 0L
  def get( l: Long ): Data = Data.NA
  def depth: Int = 0
  def hops( l: Long ): Int = 0
}

case class DataNode( offset: Long, data: Array[Double] ) extends Node {
  def first = offset
  def last = offset + size
  def size: Long = data.length
  val depth = 1
  def get( l: Long ) = {
    val i = (l-offset).toInt
    if( i < 0 || i >= size) Data.NA 
    else Data( data( i ) )
  }
  override def toString = s"DataNode($offset,$size)"
  def hops( l: Long ) = 1
}

case class BinaryNode private[track]( left: Node, right: Node ) extends Node {
  require( left.first < right.first )
  require( left.last <= right.first )
  val first: Long = left.first
  val last: Long = right.last
  val size: Long = left.size + right.size
  lazy val depth = (left.depth max right.depth) + 1
  def get( l: Long ): Data = 
    if( l < first || l >= last ) Data.NA
    else if( l < left.last ) left.get(l)
    else right.get(l)
  def hops( l: Long ) = 
    if( l < first || l >= last ) 1
    else if( l < left.last ) left.hops(l) + 1
    else right.hops(l) + 1
}

object BinaryNode {
  def build( left: Node, right: Node ): Node = (left,right) match {
    case (EmptyNode,n) => n
    case (n,EmptyNode) => n
    case (l,r) => BinaryNode(l,r)
  }
}


