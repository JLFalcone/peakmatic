# Peakmatic

Peakmatic is a peak calling software for genomic data. It allows to
predict nucleosomes locations in MNase chromatine digests.

The software can read data in the Wiggle, BigWig, or gzip compressed
Wiggle formats. It exports data in Wiggle format.

## References

The algorithm is described in the supplementary information of Kubik et al 2015. 

 * Kubik et al., _Nucleosome Stability Distinguishes Two Different
   Promoter Types at All Protein-Coding Genes in Yeast_, Molecular
   Cell, Volume 60, Issue 3, 2015, Pages 422-434, ISSN 1097-2765,
   [DOI:10.1016/j.molcel.2015.10.002](https://doi.org/10.1016/j.molcel.2015.10.002)
   
 * Kubik et al., _Sequence-Directed Action of RSC Remodeler and
   General Regulatory Factors Modulates +1 Nucleosome Position to
   Facilitate Transcription_, Molecular Cell, Volume 71, Issue 1,
   2018, Pages 89-102.e5, ISSN 1097-2765, 
   [DOI:10.1016/j.molcel.2018.05.030](https://doi.org/10.1016/j.molcel.2018.05.030)
 
   
# Usage

## Prerequisites

Peakmatic runs on GNU/Linux, MacOS and MS-Windows. The only requisite
is Java 8 or higher.

Developers wanting to build the software from source should also
install [SBT](https://www.scala-sbt.org/).


## Running from JAR

The executable jar files can be downloaded from the
[project repository](https://gitlab.unige.ch/JLFalcone/peakmatic/tree/master/realeases)

The file is executable without installation. Copy it to the
location and launch it with java.


## Building from source

You can build the file from source with the following commands:

```
$ cd peakmatic
$ sbt
sbt:peakmatic> compile
sbt:peakmatic> assembly
```

The first build will be long because sbt will download all
dependencies. Be patient, successive buolds will be faster.


# Further information

## Design notes

- The software was developed in Scala, with a compromise between
  performances and abstraction design.
- Dedicated datastructures were developed to represent genomic tracks
  and account form both dense or sparse data (see `track.Node`).
- GUI was done in java Swing. It is rather ugly but should work on
  every platform.



## Issues and contributions

You can report bugs and issues, or ask for features using the project
[issue tracker](https://gitlab.unige.ch/JLFalcone/peakmatic/issues).

Don't hesitate to contribute, merge requests are welcome.

## Authors

* Jean-Luc Falcone: algorithm design, developement
* Slawomir Kubik: biological expertise and algorithm design
* David Shore: biological expertise and algorithm design

## License

The software is provided with a GPL 3 license. You can use it,
redistribute it, modify it, but all copy and derivative should keep
the same license. For legal details see the `LICENSE.txt` file.

If you need a more permissive licence, please contact us.



