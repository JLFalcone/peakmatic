/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui

import scala.swing._
import scala.swing.event._

import java.io.File

import ch.unige.peakmatic.util.Konsole

object PeakFindPage{
  def apply( prefs: Prefs ): TabbedPane.Page = {
    val pfi = new PeakFindInstance( prefs )
    new TabbedPane.Page( "PeakFind", pfi.top )
  }
}

class PeakFindInstance( protected val prefs: Prefs ) extends Controller[PeakLocator] {
  
  protected def freshModel: PeakLocator = PeakLocator( 
    limit=prefs.peakLocator.limit, 
    threshold=prefs.peakLocator.threshold 
  )

  protected def redraw( model: PeakLocator ): Unit = {
    inputFile.text = model.input.map( _.toString ).getOrElse("Select the input file...")
    outputFile.text = model.output.map( _.toString ).getOrElse("Select the output file...")
    limitValue.text = model.limit.toString
    thresholdValue.text = model.threshold.toString
  }


  protected def saveParameters( model: PeakLocator ): Unit = {
    prefs.peakLocator.limit = model.limit
    prefs.peakLocator.threshold = model.threshold
  }

  lazy val inputLabel = new Label {
    text = "Input File:"
  }
  lazy val inputFile = new TextField {
    text = ""
    preferredSize = new Dimension(300,20)
    focusable = false
    tooltip = "Input file. Should be in Wiggle or in BIG WIG"
  }
  lazy val inputButton = new Button {
    text = "Choose"
  }
  lazy val outputLabel = new Label {
    text = "Output File:"
  }
  lazy val outputFile = new TextField {
    text = ""
    preferredSize = new Dimension(300,20)
    focusable = false
    tooltip = "Output file. Will be written in Wiggle format"
  }
  lazy val outputButton = new Button {
    text = "Choose"
  }
  lazy val limitLabel = new Label {
    text = "Limit:"
  }
  lazy val limitValue = new TextField {
    text = ""
    preferredSize = new Dimension(100,20)
    tooltip = "Minimum separtion between two peaks. Must be a positive integer."
  }
  lazy val thresholdLabel = new Label {
    text = "Threshold:"
  }
  lazy val thresholdValue = new TextField {
    text = ""
    preferredSize = new Dimension(100,20)
    tooltip = "Minimum peak intensity."
  }

  lazy val formPanel = new GridBagPanel {
    import GridBagPanel.Anchor._
    def c( row: Int, col: Int, anch: GridBagPanel.Anchor.Value ) = new Constraints {
      grid = (col,row)
      anchor = anch
      insets = new Insets(5,5,5,5)
    }
    layout(inputLabel) = c(0,0,East)
    layout(inputFile) = c(0,1,West)
    layout(inputButton) = c(0,2,West)
    layout(outputLabel) = c(1,0,East)
    layout(outputFile) = c(1,1,West)
    layout(outputButton) = c(1,2,West)
    layout(limitLabel) = c(2,0,East)
    layout(limitValue) = c(2,1,West)
    layout(thresholdLabel) = c(3,0,East)
    layout(thresholdValue) = c(3,1,West)
    
    border = Swing.EmptyBorder(30, 30, 30, 30)
    
    listenTo(inputButton,outputButton,limitValue,thresholdValue)
    
    reactions += {
      case ButtonClicked(`inputButton`) =>
	val inFc = makeInFC( prefs.lastDirectory )
	inFc.showOpenDialog( contents.head ) match {
	  case FileChooser.Result.Approve => 
	    val f =  inFc.selectedFile
	    updateModel( _.setInput(f), "File error", contents.head )
	    prefs.lastDirectory = f.getParent
	    doRedraw()
	  case _ =>
	}
      case ButtonClicked(`outputButton`) =>
	val outFc = makeOutFC( prefs.lastDirectory )
	outFc.showSaveDialog( contents.head ) match {
	  case FileChooser.Result.Approve => 
	    val f =  outFc.selectedFile
	    prefs.lastDirectory = f.getParent
	    if( f.exists ) {
	      val res = Dialog.showConfirmation(
		parent = contents.head,
		title = "Output file exists",
		message = s"Output file $f exists. Do you want to overwrite it ?",
		optionType = Dialog.Options.OkCancel

	      )
	      if( res == Dialog.Result.Ok ) {
		updateModel( _.setOutput(f), "File error", contents.head )
		doRedraw()
	      }
	    }  else {
	      updateModel( _.setOutput(f), "File error", contents.head )
	      doRedraw()
	    }
	  case _ =>
	}
      case EditDone(`limitValue`) => 
	updateModel( _.setLimit( limitValue.text ), "Value error", contents.head )
	doRedraw()
      case EditDone(`thresholdValue`) => 
	updateModel( _.setThreshold( thresholdValue.text ), "Value error", contents.head )
	doRedraw()
    }
    
  }


}
