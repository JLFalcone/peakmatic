/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui

import java.util.prefs.Preferences

class Prefs {

  private val prefs = Preferences.userNodeForPackage(getClass)
  private val lastDirectoryKey = "lastDirectory"
  def lastDirectory: String = 
    prefs.get( lastDirectoryKey, FactoryPrefs.lastDirectory )
  def lastDirectory_=( s: String ): Unit = 
    prefs.put( lastDirectoryKey, s )

  object peakLocator {
    private val limitKey = "peaklocator-limit"
    def limit: Int =
      prefs.getInt( limitKey, FactoryPrefs.peakLocator.limit )
    def limit_=( i: Int ): Unit =
      prefs.putInt( limitKey, i )
    private val thresholdKey = "peaklocator-threshol"
    def threshold: Double =
      prefs.getDouble( thresholdKey, FactoryPrefs.peakLocator.threshold )
    def threshold_=( d: Double ): Unit =
      prefs.putDouble( thresholdKey, d )
  }

  object fragility {
    private val limitKey = "fragility-limit"
    def limit: Int =
      prefs.getInt( limitKey, FactoryPrefs.fragility.limit )
    def limit_=( i: Int ): Unit =
      prefs.putInt( limitKey, i )
    private val minRatioKey = "fragility-minRatio"
    def minRatio: Double =
      prefs.getDouble( minRatioKey, FactoryPrefs.fragility.minRatio )
    def minRatio_=( d: Double ): Unit =
      prefs.putDouble( minRatioKey, d )
  }


}

object FactoryPrefs {
  val lastDirectory = "."
  object peakLocator {
    val limit = 100
    val threshold = 5.0
  }
  object fragility {
    val limit = 100
    val minRatio = 3.5
  }

}
