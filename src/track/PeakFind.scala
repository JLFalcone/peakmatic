/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

import scala.concurrent._
import ch.unige.peakmatic.util.Konsole

object PeakFind extends GenTrackOps {

  type MergePeaks = Seq[Double] => Double

  val MaxMergePeaks: MergePeaks = _.max

  def apply( track: Track, limit: Int, threshold: Double )(implicit K: Konsole): Track = {
    val sequential = findPeaks(limit,threshold) andThen mergeClose(limit)(MaxMergePeaks)
    sequential(track)
  }

  def mergeClose( limit: Int )( f: MergePeaks )(implicit K: Konsole): Track=>Track =
    modifyNodes( "merge-close" )( NodeOps.mergeClose( _, limit )(f) )
    
  def findPeaks( width: Int, threshold: Double )(implicit K: Konsole): Track=>Track = 
    modifyNodes( "find-peaks" )( NodeOps.findPeaks( _, width, threshold ) )

  def findPeaksF( width: Int, threshold: Double )( 
    implicit ec: ExecutionContext 
  ): Track=>Future[Track] = 
    modifyNodesF( "find-peaks-F" ){ (n,m) => 
      NodeOps.findPeaksF( width, threshold, m ).apply(n)
    }

  def findPeaksF2( width: Int, threshold: Double )( 
    implicit ec: ExecutionContext 
  ): Track=>Future[Track] = 
    modifyNodesF( "find-peaks-F2" ){ (n,m) => 
      NodeOps.findPeaksF2( width, threshold, m ).apply(n)
    }


  def mergeCloseF( limit: Int )( f: Seq[Double]=>Double )( 
    implicit ec: ExecutionContext 
  ): Track=>Future[Track] = 
    modifyNodesF( "merge-close-F" ){ (n,m) => 
      NodeOps.mergeCloseF( limit, m )(f).apply(n) 
    }


}
