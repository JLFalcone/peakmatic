/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

import scala.concurrent._

object NodeOps {

  def ratio( node1: Node, node2: Node, default: Double ): Node = {
    var i: Long = node1.first min node2.first
    val N: Long = node1.last max node2.last
    val nb = new NodeBuilder()
    while( i < N ) {
      val d1 = node1.get(i)
      val d2 = node2.get(i)
      if( d1.isDefined || d2.isDefined  ) {
	val x1 = d1.getOrElse( default )
	val x2 = d2.getOrElse( default )
	nb.add( i, Data(x1/x2) )
      }
      i += 1
    }
    nb.build
  }

  def filterValues( node: Node )( f: Double=>Boolean ): Node = {
    var i: Long = node.first
    val N: Long = node.last
    val nb = new NodeBuilder()
    while( i < N ) {
      val d = node.get(i)
      if( d.isDefined && f(d.value ) ) {
	nb.add( i, d )
      }
      i += 1
    }
    nb.build
  }

  def mergeClose( node: Node, limit: Int )( f: Seq[Double] => Double ): Node = {
    
    def mergeAndAdd( nb: NodeBuilder, similar: List[(Long,Double)] ): Unit = {
      if( similar.nonEmpty ) {
	//if( similar.size >= 2 ) { println( s"Merging size: ${similar.size}" ) }
	val (js,vals) = similar.unzip
	val j = js.sum / js.size
	val value = Data( f(vals) )
	//println( "ADDING" )
	nb.add( j, value )
      }
    }
    
    def mergeNodeStream( vs: Stream[(Long,Double)] ): Node = {
      val nb = new NodeBuilder()
      var similar = List[(Long,Double)]()
      //println( "SIZE OF STREAM: " + vs.size )
      vs.foreach { v =>
	//println( s"VALUE: ${v._1} ${v._2}" )
	if( similar.isEmpty || ( v._1 - similar.head._1 <= limit ) ) {
	  similar ::= v
	} else {
	  mergeAndAdd( nb, similar )
	  similar = List( v )
	}
      }
      mergeAndAdd( nb, similar )
      nb.build()
    }

    mergeNodeStream( node.toStream )

  }

  def findPeaks2( node: Node, width: Int, threshold: Double, iniFirst: Long, iniLast: Long ): Node = {
    val halfW = width/2
    var i = iniFirst max (node.first+halfW)
    val N = iniLast min (node.last-halfW)
    val nb = new NodeBuilder()
    while( i < N ) {
      val pos = i
      val peak = node.get(pos)
      if( peak.isDefined && peak.value >= threshold) {
	var max = true
	var k = (pos - halfW) max node.first
	val to = (pos + halfW) min node.last
	while( max && k < to ) {
	  val cur = node.get(k)
	  if( cur.isDefined ) {
	    max = cur.value <= peak.value
	  }
	  k += 1
	}
	if( max ) nb.add( pos, peak )
      }
      i += 1
    }
    nb.build
  }


  def findPeaks( node: Node, width: Int, threshold: Double ): Node = {
    var i = node.first
    val N = node.last - width
    val nb = new NodeBuilder()
    while( i < N ) {
      val pos = i + width/2
      val peak = node.get(pos)
      if( peak.isDefined ) {
	    var max = true
	    var k = 0
	    while( max && k < width ) {
	      val cur = node.get(i+k)
	      if( cur.isDefined ) {
	          max = cur.value <= peak.value
	      }
	      k += 1
	    }
	    if( max && peak.value >= threshold) {
	      nb.add( pos, peak )
 	    }
      }
      i += 1
    }
    nb.build
  }


  def modifyNodeF( maxSize: Long )( f: Node=>Node )
  ( implicit ec: ExecutionContext ): Node=>Future[Node] = {
      case EmptyNode => Future(EmptyNode)
      case b @ BinaryNode( l, r ) if b.size > maxSize => {
	val lF = modifyNodeF( maxSize )( f )(ec)( l )
	val rF = modifyNodeF( maxSize )( f )(ec)( r )
	for {
	    ll <- lF
	    rr <- rF
	} yield BinaryNode.build( ll, rr )
      }
      case n => Future( f(n) )
    }

  def findPeaksF( width: Int, threshold: Double, maxSize: Long )
  ( implicit ec: ExecutionContext ): Node=>Future[Node] = 
    modifyNodeF( maxSize )( findPeaks( _, width, threshold ) )

  def mergeCloseF( limit: Int, maxSize: Long )( f: Seq[Double] => Double )
  ( implicit ec: ExecutionContext ): Node=>Future[Node] = 
    modifyNodeF( maxSize )( mergeClose( _, limit )(f) )


  def findPeaksF2( 
    width: Int, threshold: Double, maxSize: Long 
  )( implicit ec: ExecutionContext ): Node=>Future[Node] = {
  
    def findIt( first: Long, last: Long, node: Node ): Future[Node] = {
      val size = last - first
      //println( s"FINDIT: $first -> $last, size: $size, maxSize: $maxSize" ) 
      if( last-first <= maxSize ) Future( findPeaks2( node, width, threshold, first, last ) )
      else {
	val half = size/2 + first
	for {
	  p1 <- findIt( first, half, node )
	  p2 <- findIt( half, last, node )
	} yield BinaryNode.build( p1, p2 )
      }
    }

    (node:Node) => findIt( node.first, node.last, node )
  }
}
