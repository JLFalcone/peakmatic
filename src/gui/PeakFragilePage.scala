/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui

import scala.swing._
import scala.swing.event._

import java.io.File

object PeakFragilePage{
  def apply( prefs: Prefs ): TabbedPane.Page = {
    val pfi = new PeakFragInstance( prefs )
    new TabbedPane.Page( "PeakFragility", pfi.top )
  }
}

class PeakFragInstance( protected val prefs: Prefs ) extends Controller[PeakMatcher] {
  
  protected def freshModel: PeakMatcher = PeakMatcher( 
    limit=prefs.fragility.limit, 
    minRatio=prefs.fragility.minRatio
  )(tak)

  protected def redraw( model: PeakMatcher ): Unit = {
    input1File.text = model.input1.map( _.toString ).getOrElse("Select the input file 1...")
    input2File.text = model.input2.map( _.toString ).getOrElse("Select the input file 2...")
    outputFile.text = model.output.map( _.toString ).getOrElse("Select the output file...")
    limitValue.text = model.limit.toString
    minRatioValue.text = model.minRatio.toString
  }


  protected def saveParameters( model: PeakMatcher ): Unit = {
    prefs.fragility.limit = model.limit
    prefs.fragility.minRatio = model.minRatio
  }

  lazy val input1Label = new Label {
    text = "Input File 1:"
  }
  lazy val input1File = new TextField {
    text = ""
    preferredSize = new Dimension(300,20)
    focusable = false
    tooltip = "Input file. Should be in Wiggle or in BIG WIG"
  }
  lazy val input1Button = new Button {
    text = "Choose"
  }

  lazy val input2Label = new Label {
    text = "Input File 2:"
  }
  lazy val input2File = new TextField {
    text = ""
    preferredSize = new Dimension(300,20)
    focusable = false
    tooltip = "Input file. Should be in Wiggle or in BIG WIG"
  }
  lazy val input2Button = new Button {
    text = "Choose"
  }


  lazy val outputLabel = new Label {
    text = "Output File:"
  }
  lazy val outputFile = new TextField {
    text = ""
    preferredSize = new Dimension(300,20)
    focusable = false
    tooltip = "Output file. Will be written in Wiggle format"
  }
  lazy val outputButton = new Button {
    text = "Choose"
  }
  lazy val limitLabel = new Label {
    text = "Limit:"
  }
  lazy val limitValue = new TextField {
    text = ""
    preferredSize = new Dimension(100,20)
    tooltip = "Minimum separtion between two peaks. Must be a positive integer."
  }
  lazy val minRatioLabel = new Label {
    text = "Min. Ratio:"
  }
  lazy val minRatioValue = new TextField {
    text = ""
    preferredSize = new Dimension(100,20)
    tooltip = "Minimum ratio between first and second."
  }

  lazy val formPanel = new GridBagPanel {
    import GridBagPanel.Anchor._
    def c( row: Int, col: Int, anch: GridBagPanel.Anchor.Value ) = new Constraints {
      grid = (col,row)
      anchor = anch
      insets = new Insets(5,5,5,5)
    }
    layout(input1Label) = c(0,0,East)
    layout(input1File) = c(0,1,West)
    layout(input1Button) = c(0,2,West)
    layout(input2Label) = c(1,0,East)
    layout(input2File) = c(1,1,West)
    layout(input2Button) = c(1,2,West)
    layout(outputLabel) = c(2,0,East)
    layout(outputFile) = c(2,1,West)
    layout(outputButton) = c(2,2,West)
    layout(limitLabel) = c(3,0,East)
    layout(limitValue) = c(3,1,West)
    layout(minRatioLabel) = c(4,0,East)
    layout(minRatioValue) = c(4,1,West)
    
    border = Swing.EmptyBorder(30, 30, 30, 30)
    
    listenTo(input1Button,input2Button,outputButton,limitValue,minRatioValue)
    
    reactions += {
      case ButtonClicked(`input1Button`) =>
	val inFc = makeInFC( prefs.lastDirectory )
	inFc.showOpenDialog( contents.head ) match {
	  case FileChooser.Result.Approve => 
	    val f =  inFc.selectedFile
	    updateModel( _.setInput1(f), "File error", contents.head )
	    prefs.lastDirectory = f.getParent
	    doRedraw()
	  case _ =>
	}
      case ButtonClicked(`input2Button`) =>
	val inFc = makeInFC( prefs.lastDirectory )
	inFc.showOpenDialog( contents.head ) match {
	  case FileChooser.Result.Approve => 
	    val f =  inFc.selectedFile
	    updateModel( _.setInput2(f), "File error", contents.head )
	    prefs.lastDirectory = f.getParent
	    doRedraw()
	  case _ =>
	}

      case ButtonClicked(`outputButton`) =>
	val outFc = makeOutFC( prefs.lastDirectory )
	outFc.showSaveDialog( contents.head ) match {
	  case FileChooser.Result.Approve => 
	    val f =  outFc.selectedFile
	    prefs.lastDirectory = f.getParent
	    if( f.exists ) {
	      val res = Dialog.showConfirmation(
		parent = contents.head,
		title = "Output file exists",
		message = s"Output file $f exists. Do you want to overwrite it ?",
		optionType = Dialog.Options.OkCancel

	      )
	      if( res == Dialog.Result.Ok ) {
		updateModel( _.setOutput(f), "File error", contents.head )
		doRedraw()
	      }
	    }  else {
	      updateModel( _.setOutput(f), "File error", contents.head )
	      doRedraw()
	    }
	  case _ =>
	}
      case EditDone(`limitValue`) => 
	updateModel( _.setLimit( limitValue.text ), "Value error", contents.head )
	doRedraw()
      case EditDone(`minRatioValue`) => 
	updateModel( _.setMinRatio( minRatioValue.text ), "Value error", contents.head )
	doRedraw()
    }
    
  }


}
