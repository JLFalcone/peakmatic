/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

import scala.concurrent._

import ch.unige.peakmatic.util.Konsole

trait GenTrackOps {

  def modifyNodes( desc: String )( f: Node=>Node )(implicit K: Konsole): Track=>Track = { track=>
    K.info( desc, "Starting" )
    val newData = track.data.par.map {
      case (chr, node) => {
	K.info( desc, s"Computing $chr" )
	(chr, f(node))
      }
    }
    val t = track.copy( data=newData.seq )
    K.info( desc, "Done" )
    t
  }


  def modifyNodesF( desc: String )( f: (Node,Long)=>Future[Node] )( 
    implicit ec: ExecutionContext ): Track=>Future[Track] = { track=>
      println( s">>> STARTING $desc" )
      val maxSize = track.data.values.map( _.size ).min
      val data = track.data.toList
      val newDataF = Future.traverse( data ){
	case (chr, node) => {
	  println( s">>> COMPUTING $desc for $chr" )
	  f( node, maxSize ).map{ nn =>
	    println( s">>> DONE COMPUTING $desc for $chr" )
	    chr->nn
	  }
	}
      }
      newDataF.map { nd =>
	println( s">>> DONE $desc" )
	track.copy( data=nd.toMap )
      }
    }


}

object TrackOps extends GenTrackOps {


  def filterValues( f: Double=>Boolean ): Track=>Track = 
    modifyNodes( "filter-values" )( NodeOps.filterValues(_)(f) )

  def filterChromosomes( f: Chromosome=>Boolean ): Track=>Track = { track =>
    val toRemove = track.data.keySet.filterNot(f)
    val newData = track.data -- toRemove
    track.copy( data=newData )
  }



}

