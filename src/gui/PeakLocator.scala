/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui


import scala.util.{Try,Success,Failure}
import java.io.File
import track._

case class PeakLocator( 
  input: Option[File]=None,
  output: Option[File]=None,
  limit: Int = 100,
  threshold: Double = 5.0
)( implicit konsole: util.Konsole ) extends Model[PeakLocator] {
  
  def setInput( f: File ): Either[String,PeakLocator] = 
    setInFile( f )( copy( input=Some(f) ) )

  def setOutput( f: File ): Either[String,PeakLocator] = 
    setOutFile( f )( copy( output=Some(f) ) )

  def setLimit( s: String ): Either[String,PeakLocator] = {
    Try {
      s.toInt
    } match {
      case Success(i) if i > 0 => Right( copy( limit=i ) )
      case Success(i) => Left( s"Limit must be positive, received $i." )
      case Failure(_) => Left( s"Cannot convert $s to an integer." )
    }      
  }

  def setThreshold( s: String ): Either[String,PeakLocator] = {
    Try {
      s.toDouble
    } match {
      case Success(d) if d >= 0 => Right( copy( threshold=d ) )
      case Success(d) => Left( s"Limit must be positive, received $d." )
      case Failure(_) => Left( s"Cannot convert $s to a number." )
    }
  }

  def resetFiles: PeakLocator = copy( input=None, output=None )

  def validate(): Either[String,(File,File)] = (input,output) match {
    case (None,None) => Left( "Enter the input and the output files.")
    case (None,_) => Left( "Enter the input file.")
    case (_,None) => Left( "Enter the output file.")
    case (Some(in),Some(out)) => Right((in,out))
  }
  
  def run(): Either[String,Unit] = {
    for {
      inOut <- validate().right
      full <- tryOpen(inOut._1).right
      peaks <- Right( PeakFind( full, limit, threshold )(konsole) ).right
      _ <- try2either( TrackWiggle.write( inOut._2, peaks, false ) )( _.toString ).right
    } yield ()
  }


}
