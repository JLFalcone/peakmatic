/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package gui

import java.io.File
import scala.util.{Try,Success,Failure}
import track._

trait Model[M <: Model[M]] {

  protected def setInFile( f: File )( update: =>M ): Either[String,M] = {
    if( ! f.exists ) Left( s"Input file: $f does not exist" )
    else if( ! f.canRead ) Left( s"Input file: $f is not readable" )
    else Right( update )
  }
  protected def setOutFile( f: File )( update: =>M ): Either[String,M] = {
    if( f.exists && ! f.canWrite ) Left( s"Output file: $f exists and is not writable" )
    else Right( update )
  }

  protected def try2either[A,B]( t: Try[A] )( f: Throwable=>B ): Either[B,A] = {
    t match {
      case Success(a) => Right(a)
      case Failure(t) => Left( f(t) )
    }
  }

  protected def tryOpen( file: File )( implicit k: util.Konsole): Either[String,Track] = {
    TrackWiggle.read( file ) match {
      case Success(t) => Right(t)
      case Failure(_) => TrackBigWig.read( file ) match {
	case Success(t) => Right(t)
	case Failure(_) => Left( s"Cannot read $file. Check the format." )
      }
    }
  }


  def run(): Either[String,Unit]
  def resetFiles(): M

}
