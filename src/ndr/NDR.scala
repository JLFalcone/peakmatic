/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package  ndr
import track._

import java.io.PrintWriter

object Ops {

  val nucW = 150

  def extractNDRPos( regs: List[(Long,Long)] ): List[(Long,Long)] = {
    def loop( rem: List[(Long,Long)], result: List[(Long,Long)] ): List[(Long,Long)] =
      if( rem.isEmpty ) result.reverse
      else {
        val (x,y) = rem.head
        val l = y-x
        val n = math.round( l.toDouble/nucW ).toInt
        if( n < 1 || n > 4 ) loop( rem.tail, result )
        else {
          var i = n
          var from = x
          var regs = List[(Long,Long)]()
          while( i > 0 ) {
            val l_ = y - from
            val to =
              if( i == 1 ) y
              else from + math.round(l_.toDouble /i).toLong
            regs ::= (from, to)
            from = to
            i -= 1
          }
          loop( rem.tail, regs ++ result )
        }
      }
    loop( regs, Nil )

  }


  def findNDR( track: Track, trimSize: Int, minNDRSize: Int ): Map[Chromosome,List[(Long,Long)]] = {
    track.data.map {
      case (chr,n) => chr -> locateNDR( toStream(n), trimSize, minNDRSize )
    }
  }

  private def locateNDR( peaks: Stream[(Long,Double)], trimSize: Int, minNDRSize: Int ): List[(Long,Long)] = {
    val minSpread = 2*trimSize + minNDRSize

    def loop(
      last: Long, rest: Stream[(Long,Double)], regs: List[(Long,Long)]
    ): List[(Long,Long)] = {
      if( rest.isEmpty ) regs.reverse
      else {
        val next = rest.head
        if( next._1 - last <= minSpread   )
          loop( next._1, rest.tail, regs )
        else {
          val x = last + trimSize
          val y = next._1 - trimSize
          loop( next._1, rest.tail, (x,y)::regs )

        }
      }
    }
    if( peaks.isEmpty ) Nil
    else loop( peaks.head._1, peaks.tail, Nil )

  }

  def toStream( node: Node ): Stream[(Long,Double)] = {
    def loop( cur: Node ): Stream[(Long,Double)] = {
      cur match {
        case EmptyNode => Stream.empty
        case BinaryNode( l, r ) => loop(l) ++ loop(r)
        case dn: DataNode => {
          var idx = dn.first
          var els = List[(Long,Double)]()
          while( idx < dn.last ) {
            val x = dn.get(idx)
            if( x.isDefined ) {
              els = (idx,x.value) :: els
            }
            idx += 1
          }
          els.reverse.toStream
        }
      }
    }
    loop(node)
  }

  def count( node: Node, from: Long, to: Long ): Int = {
    var idx = from
    var n = 0
    while( idx < to ) {
      val x = node.get(idx)
      if( x.isDefined ) {
        n += 1
      }
      idx += 1
    }
    n
  }


  def avg( node: Node, from: Long, to: Long ): Data = {
    var idx = from
    var n = 0
    var s = 0.0
    while( idx < to ) {
      val x = node.get(idx)
      if( x.isDefined ) {
        s += x.value
        n += 1
      }
      idx += 1
    }
    if( n != 0 ) Data( s/n ) else Data.NA
  }
  
}
