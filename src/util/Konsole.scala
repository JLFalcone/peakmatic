/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package util

trait KonsoleFormatter {
  import com.github.nscala_time.time.Imports._

  private lazy val fmt = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm:ss")

  def timeStamp = fmt.print( DateTime.now )

  def formatMessage( kind: String, name: String, msg: String ): String =
    s"[$timeStamp] - $kind - ${name.toUpperCase}: $msg"
}


trait Konsole extends KonsoleFormatter {

  def printError( s: String ): Unit
  def printInfo( s: String ): Unit

  def error( name: String, msg: String ): Unit =
    printError( formatMessage( "ERROR", name, msg ) )
  def info( name: String, msg: String ): Unit =
    printInfo( formatMessage( "INFO", name, msg ) )
}

object Konsole {

  implicit val ConsoleKonsole = new Konsole {
    def printError( s: String ) = Console.err.println(s)
    def printInfo( s: String ) = Console.out.println(s)
    override def toString = "ConsoleKonsole"
  }

}



