organization := "ch.unige"

name := "peakmatic"

version := "1.0.0"

scalaVersion := "2.12.8"

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "com.github.nscala-time" %% "nscala-time" % "2.22.0",	
  "org.scala-lang" % "scala-reflect" % "2.12.8",	 
  "org.scala-lang.modules" %% "scala-swing" % "2.1.0"
)

scalacOptions ++= Seq("-deprecation", "-feature")

scalaSource in Compile := baseDirectory.value / "src"


