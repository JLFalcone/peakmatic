/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

case class Chromosome( name: String ) extends AnyVal

case class DataPoint( chromosome: Chromosome, start: Long, end: Long, value: Double ) {
  def mapValue( f: Double=>Double ) = copy( value=f(value) )

  def samePos( that: DataPoint ): Boolean = start==that.start && end==that.end 

  def overlaps( that: DataPoint ): Boolean =
    ( this.start <= that.start && that.start <= this.end ) ||
    ( that.start <= this.start && this.start <= that.end )

  def startsBefore( that: DataPoint ): Boolean =
    this.start < that.start

  def endsBefore( that: DataPoint ): Boolean =
    this.end < that.end


  def splitAt( l: Long ) = ( copy( end=l ), copy( start=l) )

}
