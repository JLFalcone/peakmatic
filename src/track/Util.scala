/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

import scala.util.Try

object Util {
  //FIXME: Remove it
/*
  def wiggle2binary( inFile: String, outFile: String ): Try[Unit] = 
    for{
      track <- TrackWiggle.read( inFile )
      _ <- TrackBinary.write( outFile, track )
    } yield ()
 */
}
/*
object Wiggle2Binary extends App {

  val in = args(0)
  val out = args(1)

  Util.wiggle2binary( in, out ).get

}
*/
