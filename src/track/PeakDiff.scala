/*
 * Copyright (C) 2014-2019, University of Geneva
 *
 * This file is part of Peakmatic.
 *
 * Peakmatic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Peakmatic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Peakmatic.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package ch.unige.peakmatic
package track

sealed trait PeakMatch {
  def shift: Option[Int] = this match {
    case Both( l, _, r, _ ) => Some((r-l).toInt)
    case _ => None
  }
  def ratio: Option[Double] = this match {
    case Both( _, l, _, r ) => Some(l/r)
    case _ => None
  }
}
case class Both( 
  leftPos: Long, 
  leftValue: Double,
  rightPos: Long,
  rightValue: Double
) extends PeakMatch
case class LeftOnly( 
  pos: Long, 
  value: Double
) extends PeakMatch
case class RightOnly( 
  pos: Long, 
  value: Double
) extends PeakMatch

object PeakDiff {

  type Pair = (Long,Double)
  type Pairs = Stream[Pair]

  def apply( nLeft: Node, nRight: Node, limit: Int ): Seq[PeakMatch] = {
    def cmp( left: Pairs, right: Pairs, found: Seq[PeakMatch] ): Seq[PeakMatch] =
      if( left.isEmpty && right.isEmpty ) found
      else if( left.isEmpty ) found ++ right.map( pv => RightOnly(pv._1, pv._2 ) )
      else if( right.isEmpty ) found ++ left.map( pv => LeftOnly(pv._1, pv._2 ) )
      else {
	val (pL,vL) = left.head
	val (pR,vR) = right.head
	if( math.abs(pL - pR) <= limit ) 
	  cmp( left.tail, right.tail, found :+ Both( pL,vL, pR,vR ) )
	else if( pL < pR )
	  cmp( left.tail, right, found :+ LeftOnly(pL,vL) )
	else
	  cmp( left, right.tail, found :+ RightOnly(pR,vR) )
      }

    cmp( nLeft.toStream, nRight.toStream, Vector() )
  }
  
  def apply( tLeft: Track, tRight: Track, limit: Int ): Map[Chromosome,Seq[PeakMatch]] = {
    val chrs = tLeft.chromosomes intersect tRight.chromosomes
    chrs.map{ chr => 
      chr->apply( tLeft.data(chr), tRight.data(chr), limit ) 
    }.toMap
  }

  def fragileTrack( 
    tLeft: Track, tRight: Track, limit: Int, minRatio: Double 
  ): Track = {
    val diffMap = apply( tLeft, tRight, limit )
    val data = diffMap.mapValues{ diff =>
      val nb = new NodeBuilder
      diff.foreach {
	case b @ Both(_,_,pos,_) => {
	  val r = b.ratio.get
	  if( r > minRatio )
	    nb.add( pos, Data(r) )
	}
	case LeftOnly(pos,_) => nb.add( pos, Data(10) )
	//case RightOnly(pos,_)  => nb.add( pos, Data(-9) )
	case _ =>
      }
      nb.build
    }
    Track( "Diff", s"${tLeft.name}-${tRight.name}", data )
  }

}
